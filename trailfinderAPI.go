package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net/http"
	"sort"
	"strconv"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var userLat float64 = 0
var userLon float64 = 0

// MyMux boilerplate struct definition
type MyMux struct {
}

// Point represents a location on earth
type Point struct {
	Latitude  float64
	Longitude float64
}

// DBResult for "/" endpoint
type DBResult struct {
	Greeting string
}

// Hazard will represent a point and message on a map
type Hazard struct {
	ID      bson.ObjectId `bson:"_id,omitempty"`
	Message string
	Lat     float64
	Lon     float64
	Zone    int
	Hours   int
	Delete  time.Time
}

// Trail ...
type Trail struct {
	ID         bson.ObjectId `bson:"_id,omitempty"`
	Ascent     int
	City       string
	Descent    int
	Difficulty string
	Distance   float64
	High       int
	Lat        float64
	Lon        float64
	Low        int
	Maxgrade   float64
	Mingrade   float64
	Name       string
	Park       string
	State      string
	Type       string
	Zone       int
}

// Review ...
type Review struct {
	ID       bson.ObjectId `bson:"_id,omitempty"`
	Trailid  string
	Userid   string
	Username string
	Text     string
	Rating   int
	Date     string
}

// Favorite ...
type Favorite struct {
	ID      bson.ObjectId `bson:"_id,omitempty"`
	Userid  string
	Trailid string
}

//Photo ...
type Photo struct {
	ID      bson.ObjectId `bson:"_id,omitempty"`
	Trailid string
	Path    string
}

// ByDistance ...
type ByDistance []Trail

func (a ByDistance) Len() int {
	return len(a)
}
func (a ByDistance) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}
func (a ByDistance) Less(i, j int) bool {
	return distanceBetweenPoints(a[i].Lat, a[i].Lon, userLat, userLon) < distanceBetweenPoints(a[j].Lat, a[j].Lon, userLat, userLon)
}

// HByDistance ...
type HByDistance []Hazard

func (a HByDistance) Len() int {
	return len(a)
}
func (a HByDistance) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}
func (a HByDistance) Less(i, j int) bool {
	return distanceBetweenPoints(a[i].Lat, a[i].Lon, userLat, userLon) < distanceBetweenPoints(a[j].Lat, a[j].Lon, userLat, userLon)
}

func getZone(lat, lon float64) int {
	if lat > 43 || lat < 32 || lon < -125 || lon > -114 {
		return -1
	} else if lat > 41 && lon < -122 {
		return 0
	} else if lat > 41 && lon > -122 {
		return 1
	} else if lat > 40 && lon < -122 {
		return 2
	} else if lat > 40 && lon > -122 {
		return 3
	} else if lat > 39 && lon < -122 {
		return 4
	} else if lat > 39 && lon > -122 {
		return 5
	} else if lat > 38 && lon < -121 {
		return 6
	} else if lat > 38 && lon > -121 {
		return 7
	} else if lat > 37 && lon < -120 {
		return 8
	} else if lat > 37 && lon > -120 {
		return 9
	} else if lat > 36 && lon < -119 {
		return 10
	} else if lat > 36 && lon > -119 {
		return 11
	} else if lat > 35 && lon < -118 {
		return 12
	} else if lat > 35 && lon > -118 {
		return 13
	} else if lat > 34 && lon < -116 {
		return 14
	} else if lat > 34 && lon > -116 {
		return 15
	} else if lat > 33 && lon < -116 {
		return 16
	} else if lat > 33 && lon > -116 {
		return 17
	} else if lon < -116 {
		return 18
	} else if lon > -116 {
		return 19
	} else {
		return -1
	}
}

func nearestPointInZone(zone int, lat float64, lon float64) Point {
	var p Point
	if zone == 0 {
		if lon > -122 {
			p.Longitude = -122
		} else {
			p.Longitude = lon
		}
		if lat < 41 {
			p.Latitude = 41
		} else if lat > 42 {
			p.Latitude = 42
		} else {
			p.Latitude = lat
		}
	} else if zone == 1 {
		if lon < -122 {
			p.Longitude = -122
		} else {
			p.Longitude = lon
		}
		if lat < 41 {
			p.Latitude = 41
		} else if lat > 42 {
			p.Latitude = 42
		} else {
			p.Latitude = lat
		}
	} else if zone == 2 {
		if lon > -122 {
			p.Longitude = -122
		} else {
			p.Longitude = lon
		}
		if lat < 40 {
			p.Latitude = 40
		} else if lat > 41 {
			p.Latitude = 41
		} else {
			p.Latitude = lat
		}
	} else if zone == 3 {
		if lon < -122 {
			p.Longitude = -122
		} else {
			p.Longitude = lon
		}
		if lat < 40 {
			p.Latitude = 40
		} else if lat > 41 {
			p.Latitude = 41
		} else {
			p.Latitude = lat
		}
	} else if zone == 4 {
		if lon > -122 {
			p.Longitude = -122
		} else {
			p.Longitude = lon
		}
		if lat < 39 {
			p.Latitude = 39
		} else if lat > 40 {
			p.Latitude = 40
		} else {
			p.Latitude = lat
		}
	} else if zone == 5 {
		if lon < -122 {
			p.Longitude = -122
		} else {
			p.Longitude = lon
		}
		if lat < 39 {
			p.Latitude = 39
		} else if lat > 40 {
			p.Latitude = 40
		} else {
			p.Latitude = lat
		}
	} else if zone == 6 {
		if lon > -121 {
			p.Longitude = -121
		} else {
			p.Longitude = lon
		}
		if lat < 38 {
			p.Latitude = 38
		} else if lat > 39 {
			p.Latitude = 39
		} else {
			p.Latitude = lat
		}
	} else if zone == 7 {
		if lon < -121 {
			p.Longitude = -121
		} else {
			p.Longitude = lon
		}
		if lat < 38 {
			p.Latitude = 38
		} else if lat > 39 {
			p.Latitude = 39
		} else {
			p.Latitude = lat
		}
	} else if zone == 8 {
		if lon > -120 {
			p.Longitude = -120
		} else {
			p.Longitude = lon
		}
		if lat < 37 {
			p.Latitude = 37
		} else if lat > 38 {
			p.Latitude = 38
		} else {
			p.Latitude = lat
		}
	} else if zone == 9 {
		if lon < -120 {
			p.Longitude = -120
		} else {
			p.Longitude = lon
		}
		if lat < 37 {
			p.Latitude = 37
		} else if lat > 38 {
			p.Latitude = 38
		} else {
			p.Latitude = lat
		}
	} else if zone == 10 {
		if lon > -119 {
			p.Longitude = -119
		} else {
			p.Longitude = lon
		}
		if lat < 36 {
			p.Latitude = 36
		} else if lat > 37 {
			p.Latitude = 37
		} else {
			p.Latitude = lat
		}
	} else if zone == 11 {
		if lon < -119 {
			p.Longitude = -119
		} else {
			p.Longitude = lon
		}
		if lat < 36 {
			p.Latitude = 36
		} else if lat > 37 {
			p.Latitude = 37
		} else {
			p.Latitude = lat
		}
	} else if zone == 12 {
		if lon > -118 {
			p.Longitude = -118
		} else {
			p.Longitude = lon
		}
		if lat < 35 {
			p.Latitude = 35
		} else if lat > 36 {
			p.Latitude = 36
		} else {
			p.Latitude = lat
		}
	} else if zone == 13 {
		if lon < -118 {
			p.Longitude = -118
		} else {
			p.Longitude = lon
		}
		if lat < 35 {
			p.Latitude = 35
		} else if lat > 36 {
			p.Latitude = 36
		} else {
			p.Latitude = lat
		}
	} else if zone == 14 {
		if lon > -116 {
			p.Longitude = -116
		} else {
			p.Longitude = lon
		}
		if lat < 34 {
			p.Latitude = 34
		} else if lat > 35 {
			p.Latitude = 35
		} else {
			p.Latitude = lat
		}
	} else if zone == 15 {
		if lon < -116 {
			p.Longitude = -116
		} else {
			p.Longitude = lon
		}
		if lat < 34 {
			p.Latitude = 34
		} else if lat > 35 {
			p.Latitude = 35
		} else {
			p.Latitude = lat
		}
	} else if zone == 16 {
		if lon > -116 {
			p.Longitude = -116
		} else {
			p.Longitude = lon
		}
		if lat < 33 {
			p.Latitude = 33
		} else if lat > 34 {
			p.Latitude = 34
		} else {
			p.Latitude = lat
		}
	} else if zone == 17 {
		if lon < -116 {
			p.Longitude = -116
		} else {
			p.Longitude = lon
		}
		if lat < 33 {
			p.Latitude = 33
		} else if lat > 34 {
			p.Latitude = 34
		} else {
			p.Latitude = lat
		}
	} else if zone == 18 {
		if lon > -116 {
			p.Longitude = -116
		} else {
			p.Longitude = lon
		}
		if lat < 32 {
			p.Latitude = 32
		} else if lat > 33 {
			p.Latitude = 33
		} else {
			p.Latitude = lat
		}
	} else if zone == 19 {
		if lon < -116 {
			p.Longitude = -116
		} else {
			p.Longitude = lon
		}
		if lat < 32 {
			p.Latitude = 32
		} else if lat > 33 {
			p.Latitude = 33
		} else {
			p.Latitude = lat
		}
	}
	return p
}

func filterFunc(trails []Trail, distance float64, la float64, lo float64) []Trail {
	var index int = 0
	for index < len(trails) && distanceBetweenPoints(la, lo, trails[index].Lat, trails[index].Lon) < distance {
		index++
	}

	return trails[0:index]
}

func filterHazards(hazards []Hazard, distance float64, la float64, lo float64) []Hazard {
	var index int = 0
	for index < len(hazards) && distanceBetweenPoints(la, lo, hazards[index].Lat, hazards[index].Lon) < distance {
		index++
	}

	return hazards[0:index]
}

func hsin(theta float64) float64 {
	return math.Pow(math.Sin(theta/2), 2)
}

// Pretty self explanatory. Found this code on stack overflow and changed it to return miles
func distanceBetweenPoints(lat1 float64, lon1 float64, lat2 float64, lon2 float64) float64 {
	var la1, lo1, la2, lo2, r, meters float64
	la1 = lat1 * math.Pi / 180
	lo1 = lon1 * math.Pi / 180
	la2 = lat2 * math.Pi / 180
	lo2 = lon2 * math.Pi / 180

	r = 6378100 // Earth radius in meters

	// calculate
	h := hsin(la2-la1) + math.Cos(la1)*math.Cos(la2)*hsin(lo2-lo1)

	meters = 2 * r * math.Asin(math.Sqrt(h))
	return math.Abs(meters * 0.000621371) // convert meters to miles
}

// Takes in a distance and lat/lon and returns all zones within distance of that point.
func getNearbyZones(distance float64, lat float64, lon float64) []int {
	var results [21]int
	var resultCount int
	var nearest Point
	results[0] = getZone(lat, lon)
	resultCount = 1

	for i := 0; i < 20; i++ {
		nearest = nearestPointInZone(i, lat, lon)
		if i != results[0] && distanceBetweenPoints(lat, lon, nearest.Latitude, nearest.Longitude) < distance {
			results[resultCount] = i
			resultCount++
		}
	}

	return results[0:resultCount]
}

func getHazards(distance float64, la float64, lo float64) []Hazard {
	session, err := mgo.Dial("127.0.0.1:27017")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("TrailFinder").C("Hazards")

	now := time.Now()

	err = c.Remove(bson.M{"delete": bson.M{"$lte": now}})

	var hazards []Hazard

	if distance == -1 {
		err = c.Find(bson.M{}).All(&hazards)

		if err != nil {
			log.Fatal(err)
		}
	} else {
		err = c.Find(bson.M{"zone": bson.M{"$in": getNearbyZones(distance, la, lo)}}).All(&hazards)

		if err != nil {
			log.Fatal(err)
		}

		userLat = la
		userLon = lo
		sort.Sort(HByDistance(hazards))
		hazards = filterHazards(hazards, distance, la, lo)
	}

	fmt.Println(hazards)
	return hazards
}

func getReviews(id string) []Review {
	session, err := mgo.Dial("127.0.0.1:27017")

	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("TrailFinder").C("Reviews")

	var reviews []Review

	err = c.Find(bson.M{"trailid": id}).All(&reviews)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(reviews)
	return reviews
}

func getPhotos(id string) []Photo {
	session, err := mgo.Dial("127.0.0.1:27017")

	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("TrailFinder").C("Photos")

	var photos []Photo

	err = c.Find(bson.M{"trailid": id}).All(&photos)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(photos)
	return photos
}

func deleteHazard(w http.ResponseWriter, r *http.Request) {
	var h Hazard

	if r.Body == nil {
		http.Error(w, "Please send a request body", 400)
		return
	}

	err := json.NewDecoder(r.Body).Decode(&h)

	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	//h = getHazards()[0]

	session, err := mgo.Dial("127.0.0.1:27017")

	if err != nil {
		http.Error(w, err.Error(), 400)
	}

	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("TrailFinder").C("Hazards")

	err = c.Remove(bson.M{"_id": h.ID})

	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	fmt.Fprintf(w, "Hazard Successfully Deleted!")
	return
}

// Function for getting trails. If distance is -1, it returns all trails in the database,
// otherwise it returns trails within the distance specified from the point specified.
func getTrails(distance float64, la float64, lo float64) []Trail {
	session, err := mgo.Dial("127.0.0.1:27017")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("TrailFinder").C("Trails")

	var trails []Trail

	if distance == -1 {
		err = c.Find(bson.M{}).All(&trails)

		if err != nil {
			log.Fatal(err)
		}

		sort.Sort(ByDistance(trails))

	} else {
		err = c.Find(bson.M{"zone": bson.M{"$in": getNearbyZones(distance, la, lo)}}).All(&trails)
		userLat = la
		userLon = lo
		sort.Sort(ByDistance(trails))
		trails = filterFunc(trails, distance, la, lo)
	}

	fmt.Println(trails)
	return trails
}

func getFavorites(userid string) []Favorite {
	session, err := mgo.Dial("127.0.0.1:27017")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("TrailFinder").C("Favorites")

	var favorites []Favorite

	err = c.Find(bson.M{"userid": userid}).All(&favorites)
	if err != nil {
		panic(err)
	}

	return favorites
}

func getFavoriteTrails(favorites []Favorite) []Trail {
	session, err := mgo.Dial("127.0.0.1:27017")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("TrailFinder").C("Trails")

	ids := make([]bson.ObjectId, len(favorites))

	for i := 0; i < len(favorites); i++ {
		ids[i] = bson.ObjectIdHex(favorites[i].Trailid)
	}
	fmt.Println(ids)

	var trails []Trail

	err = c.Find(bson.M{"_id": bson.M{"$in": ids}}).All(&trails)

	if err != nil {
		panic(err)
	}

	fmt.Println(trails)

	return trails
}

func getGreeting() string {
	session, err := mgo.Dial("127.0.0.1:27017")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("TrailFinder").C("message")

	result := DBResult{}
	err = c.Find(bson.M{"_id": bson.ObjectIdHex("5ad955dbec6738e233793dc8")}).One(&result)
	if err != nil {
		log.Fatal(err)
	}

	return result.Greeting
}

func postHazard(w http.ResponseWriter, r *http.Request) {
	var h Hazard
	if r.Body == nil {
		http.Error(w, "Please send a request body", 400)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&h)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	if h.Lat < -90 || h.Lat > 90 {
		http.Error(w, "Invalid Latitude", 400)
		return
	}
	if h.Lon < -180 || h.Lon > 180 {
		http.Error(w, "Invalid Longitude", 400)
		return
	}

	session, err := mgo.Dial("127.0.0.1:27017")

	if err != nil {
		panic(err)
	}

	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("TrailFinder").C("Hazards")

	h.Zone = getZone(h.Lat, h.Lon)
	h.Delete = time.Now().Add(time.Hour * time.Duration(h.Hours))

	err = c.Insert(&h)

	if err != nil {
		log.Fatal(err)
	}

	return
}

func postReview(w http.ResponseWriter, r *http.Request) {
	var rv Review
	if r.Body == nil {
		http.Error(w, "Please send a request body", 400)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&rv)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	if rv.Rating < 1 || rv.Rating > 5 {
		http.Error(w, "Invalid Rating", 400)
		return
	}

	session, err := mgo.Dial("127.0.0.1:27017")

	if err != nil {
		panic(err)
	}

	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("TrailFinder").C("Reviews")

	rv.Date = time.Now().String()

	fmt.Println("Date: ", rv.Date)

	err = c.Insert(&rv)

	if err != nil {
		log.Fatal(err)
	}

	return
}

func postPhoto(w http.ResponseWriter, r *http.Request) {
	var ph Photo
	if r.Body == nil {
		http.Error(w, "Please send a request body", 400)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&ph)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	session, err := mgo.Dial("127.0.0.1:27017")

	if err != nil {
		panic(err)
	}

	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("TrailFinder").C("Photos")

	err = c.Insert(&ph)

	if err != nil {
		log.Fatal(err)
	}

	return
}

// This is for posting a new favorite trail to the database.
func postFavorite(w http.ResponseWriter, r *http.Request) {
	var fv Favorite
	if r.Body == nil {
		http.Error(w, "Please send a request body", 400)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&fv)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	session, err := mgo.Dial("127.0.0.1:27017")
	if err != nil {
		panic(err)
	}

	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("TrailFinder").C("Favorites")

	var favorites []Favorite

	err = c.Find(bson.M{"userid": fv.Userid, "trailid": fv.Trailid}).All(&favorites)

	if len(favorites) == 0 {
		err = c.Insert(&fv)

		if err != nil {
			log.Fatal(err)
		}
	}

	return
}

func deleteFavorite(w http.ResponseWriter, r *http.Request) {
	var fv Favorite
	if r.Body == nil {
		http.Error(w, "Please send a request body", 400)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&fv)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	session, err := mgo.Dial("127.0.0.1:27017")
	if err != nil {
		panic(err)
	}

	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("TrailFinder").C("Favorites")

	info, err := c.RemoveAll(bson.M{"userid": fv.Userid, "trailid": fv.Trailid})

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(info)

	return
}

// This is the router. All new requests come through here.
func (p *MyMux) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/" {
		if r.Method == "GET" {
			fmt.Fprintf(w, getGreeting())
			return
		}
		if r.Method == "POST" {
			fmt.Fprintf(w, "Post")
			return
		}
	}
	if r.URL.Path == "/Reviews" {
		if r.Method == "GET" {
			var reviews []Review

			if r.URL.Query()["trailid"] == nil {
				fmt.Fprintf(w, "trailid not found")
				return
			}

			id := r.URL.Query()["trailid"][0]

			reviews = getReviews(id)

			result, err := json.Marshal(reviews)

			if err != nil {
				fmt.Println("json err:", err)
				return
			}
			if string(result) == "null" {
				fmt.Fprintf(w, "[]")
			} else {
				fmt.Fprintf(w, string(result))
			}
			return
		}
		if r.Method == "POST" {
			postReview(w, r)
			return
		}
	}
	if r.URL.Path == "/Hazards" {
		if r.Method == "GET" {

			var hazards []Hazard

			if r.URL.Query()["distance"] != nil && r.URL.Query()["lat"] != nil && r.URL.Query()["lon"] != nil {

				distance, err := strconv.ParseFloat(r.URL.Query()["distance"][0], 64)

				if err != nil {
					fmt.Println("json err:", err)
					return
				}

				lat, err := strconv.ParseFloat(r.URL.Query()["lat"][0], 64)

				if err != nil {
					fmt.Println("json err:", err)
					return
				}

				lon, err := strconv.ParseFloat(r.URL.Query()["lon"][0], 64)

				if err != nil {
					fmt.Println("json err:", err)
					return
				}

				hazards = getHazards(distance, lat, lon)
			} else {
				hazards = getHazards(-1, 0, 0)
			}

			result, err := json.Marshal(hazards)

			if err != nil {
				fmt.Println("json err:", err)
				return
			}

			fmt.Fprintf(w, string(result))
			return
		}
		if r.Method == "POST" {
			postHazard(w, r)
			return
		}

		if r.Method == "DELETE" {
			deleteHazard(w, r)
			return
		}
	}
	if r.URL.Path == "/Trails" {
		if r.Method == "GET" {
			var trails []Trail
			if r.URL.Query()["distance"] != nil && r.URL.Query()["lat"] != nil && r.URL.Query()["lon"] != nil {

				distance, err := strconv.ParseFloat(r.URL.Query()["distance"][0], 64)

				if err != nil {
					fmt.Println("json err:", err)
					return
				}

				lat, err := strconv.ParseFloat(r.URL.Query()["lat"][0], 64)

				if err != nil {
					fmt.Println("json err:", err)
					return
				}

				lon, err := strconv.ParseFloat(r.URL.Query()["lon"][0], 64)

				if err != nil {
					fmt.Println("json err:", err)
					return
				}

				trails = getTrails(distance, lat, lon)
			} else {
				trails = getTrails(-1, 0, 0)
			}

			result, err := json.Marshal(trails)

			if err != nil {
				fmt.Println("json err:", err)
				return
			}

			fmt.Fprintf(w, string(result))
			return
		}
	}
	if r.URL.Path == "/Favorites" {
		if r.Method == "GET" {
			var favorites []Favorite
			var trails []Trail
			if r.URL.Query()["userid"] != nil {
				favorites = getFavorites(r.URL.Query()["userid"][0])
				fmt.Println(favorites)
				trails = getFavoriteTrails(favorites)

				result, err := json.Marshal(trails)
				if err != nil {
					panic(err)
				}
				fmt.Fprintf(w, string(result))
				return
			} else {
				fmt.Fprintf(w, "Need user id")
				return
			}

		}
		if r.Method == "POST" {
			postFavorite(w, r)
			return
		}
		if r.Method == "DELETE" {
			deleteFavorite(w, r)
			return
		}
	}
	if r.URL.Path == "/Photos" {
		if r.Method == "GET" {
			var photos []Photo

			if r.URL.Query()["trailid"] == nil {
				fmt.Fprintf(w, "trailid not found")
				return
			}

			id := r.URL.Query()["trailid"][0]

			photos = getPhotos(id)

			result, err := json.Marshal(photos)

			if err != nil {
				fmt.Println("json err:", err)
				return
			}
			if string(result) == "null" {
				fmt.Fprintf(w, "[]")
			} else {
				fmt.Fprintf(w, string(result))
			}
			return
		}
		if r.Method == "POST" {
			postPhoto(w, r)
			return
		}
	}
	http.NotFound(w, r)
	return
}

// Main just listens for connections and then passes them to the router.
func main() {
	mux := &MyMux{}
	http.ListenAndServe(":3000", mux)
}
